### cookie token session 

## cookie

cookie是服务器生成后随着响应被自动存储在浏览器，每次访问网站的时候浏览器会将该网站的cookie发回给服务器，网站可以随意更改本地的cookie
服务器可以设置的 cookie字段expires, domain, path, secure, HttpOnly
**expires**：过期时间
**domain**：设置域名，默认只发送设置它的域名和子域名下，通过domiain可以指定更高的域名层级 
**path**: path 字段定义 Cookie 可以被发送到服务器的 URL 路径。Cookie 只会在请求匹配该路径或其子路径的页面上发送。如果未指定，默认为设置 Cookie 时的页面路径

secure 字段指示 Cookie 只能通过 HTTPS 连接发送。它增强了 Cookie 的安全性，避免 Cookie 被未加密的 HTTP 请求发送，防止**中间人**攻击。

httponly：阻止js通过document.cookie访问该cookie，防止（**跨站脚本攻击**）xss获取cookie

samesite：控制是否允许第三方网站使用该cookie，**防止跨站请求伪造**（csrf）




## session
 
 服务端用session保存用户加密后的状态，客户端用cookie保存session，服务端把session种到cookie中，下次访问时，cookie携带session

 服务端下发的是sessionid。浏览器携带的也是seesiond

 session存在的一些问题，
 session需要服务器共享，比如用户在A网站进行登陆了。再去访问B，C的时候就自动登录了，如果是session，需要在不同的服务器间维护同一份session，一个更改后广播到其他的服务器，或者是存在一个共同的地方维护session，但是工作量大


 ## token
 过程：
用户通过用户名和密码发送请求
程序验证
程序返回一个签名的token给客户端
客户端储存token, 并且每次用每次发送请求
服务端验证Token并返回数据
这个防范现有的标准是JWT（JSON Web Token）
由.分割的三个部分组成
header.paylaod.signature

header存储加密方式
payload存储需要传递的数据 
sinature为 服务器根据header中声明的加密算法+服务器的密钥对headr和payload进行加密生成

**使用方式**
客户端拿到token之后，可以存储在cooike/localstorage中，客户端每次与客户端进行通信需要带上token（可以使用cookie携带，但是不能跨域，更好的方式是通过http的authorization字段携带）

JWT最大的优势是服务器不再需要存储Session，但是这个也导致了问题，无法废弃某个未过期的token，为了解决此问题，通过Access token和refresh token进行解决

refresh token有效期长，access有效期短，当access token由于过期后，使用refesh token新的token，当refresh token过期后，用户需要重新登录，refresh token及过期时间存储在服务器中

