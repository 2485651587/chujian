## BFC(块级格式上下文)
触发条件：绝对定位/fix定位
        inline-block 
        float 
        oberflow不为visible 
应用：margin传递 
     清除浮动

## 层叠上下文 
![alt text](image.png)
层叠上下级：backgrpound/border < z-index负值 < block元素 < 浮动元素 < 行内元素  < z-index为0/auto < z-index为正

## new 运算符的执行过程 

新生成一个对象 
链接原型    obj._proto_=fn.prototype 
绑定this
返回新对象（如果构造函数有自己的return，返回该值 
```js

        //

    function myNew(constructor, ...args) {
    // 1. 创建一个空对象，并将其原型链接到构造函数的 prototype
    const obj = Object.create(constructor.prototype);
    
    // 2. 执行构造函数，并将 `this` 绑定到新对象
    const result = constructor.apply(obj, args);
    
    // 3. 如果构造函数返回了对象类型的值，返回该值，否则返回新对象
    return result instanceof Object ? result : obj;
}

```



## 判断类型的几种方法

typeof 缺点：不能判断对null array object进行判断
instanceof 缺点：只能哦判断对象类型
Object.prototype.toString().call() 原理：每个对象里面都有一个【class】，用于表示该对象的类型。这是一个隐式属性，无法直接访问

## 基本数据类型和引用数据类型

基本数据类型 存储在栈中，因为它们值不可变、大小固定，且操作高效。
引用数据类型 存储在堆中，因为它们值可变、大小不固定，并通过栈中的引用进行访问。

## 原型和原型链
原型：每个对象创建的时候，都会自动与一个对象（原型）进行关联，并且每个对象都会从原型继承属性
原型链：相互关联的原型组成的链状结构
![alt text](image-1.png)