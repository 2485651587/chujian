//实现promiseFinally


class Promise {
    finally(callback) {
        return this.then(
            // finally前面是成功，就执行成功的回调，并把前面的参数向下传递
            value=> {
                // callback()执行完可能返回一个Promise
                // 也可能不返回一个Promise，为了兼容，嵌套一层Promise.resolve保证向下传递
                return Promise.resolve(callback()).then(()=> value)
            }
            // finally前面是失败，就执行失败的回调，并把前面的参数向下传递
            reason=> {
                return Promise.resolve(callback()).then(()=> throw reason)
            }
        )
    }
}

