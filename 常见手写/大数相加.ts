const bigNumber = (n1, n2) => {
    let carry = 0;
    let len = n1.length - 1;
    let len2 = n2.length - 1;
    let result = '';

    // 处理两个数字的公共部分
    while (len >= 0 || len2 >= 0) {
        const var1 = len >= 0 ? Number(n1[len]) : 0;
        const var2 = len2 >= 0 ? Number(n2[len2]) : 0;

        const sum = var1 + var2 + carry;
        result = (sum % 10) + result; // 将个位数拼接到结果前面
        carry = Math.floor(sum / 10);

        len--;
        len2--;
    }

    // 如果还有进位，添加到结果前面
    if (carry) {
        result = carry + result;
    }

    return result;
}
