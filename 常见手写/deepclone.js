//实现深拷贝，带循环引用



const deeoclone= (obj, map = new WeakMap())=> {
    if (typeof obj !== "object" || obj===null) {
      return obj;
    }
  
    if (map.get(obj)) {
      return map.get(obj)
    }
    
    const isArray = Array.isArray(obj) || Object.prototype.toString.call(obj) === '[object Array]'
    
    let result = isArray ? [] : {}
  
    map.set(obj, result)
    
 

    for(let key in Object.keys(obj)){ //只遍历自身属性
        result[key]=deeoclone(obj[key],map)
    }

    return result
  }
  
  


