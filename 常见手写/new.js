function myNew(constructor, ...args) {
    // 1. 创建一个空对象，并将其原型链接到构造函数的 prototype
    const obj = Object.create(constructor.prototype);
    
    // 2. 执行构造函数，并将 `this` 绑定到新对象
    const result = constructor.apply(obj, args);
    
    // 3. 如果构造函数返回了对象类型的值，返回该值，否则返回新对象
    return result instanceof Object ? result : obj;

}


