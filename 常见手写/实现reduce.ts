// 定义自定义的 reduce 函数作为 Array 的原型方法
Array.prototype.myReduce = function(callback, initialValue) {
    // 检查 this 是否为数组，避免 reduce 在非数组对象上使用
    if (this == null) {
      throw new TypeError("Array.prototype.myReduce called on null or undefined");
    }
    // 检查回调函数是否是函数类型
    if (typeof callback !== "function") {
      throw new TypeError(callback + " is not a function");
    }
  
    const array = this; // 保存当前数组
    let accumulator;    // 累加器
  
    let startIndex = 0; // 开始索引
  
    // 如果有提供初始值，设置 accumulator 为初始值
    if (arguments.length > 1) {
      accumulator = initialValue;
    } else {
      // 如果没有提供初始值，数组不能是空的，否则会抛出错误
      if (array.length === 0) {
        throw new TypeError("Reduce of empty array with no initial value");
      }
      // 使用数组第一个元素作为初始值
      accumulator = array[0];
      startIndex = 1; // 从第二个元素开始迭代
    }
  
    // 遍历数组元素
    for (let i = startIndex; i < array.length; i++) {
      // 执行回调函数，更新 accumulator 的值
      accumulator = callback(accumulator, array[i], i, array);
    }
  
    return accumulator; // 返回最终的累计值
  };
  