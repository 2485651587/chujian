//实现防抖和节流


/** 防抖
 * 
 * 短时间多次触发只执行一次，如果n秒后重新触发则重新开始计时
 */


const debounce = (fn, delay) => {

    let timer

    return function(args) {
        if (timer) {
            clearInterval(timer)
        }
        timer = setTimeout(() => {
            fn.apply(null, [...args])


        }, delay);
    }


}





/**
 * ns内只执行一次  
 */



const throttle = (fn, delay) => {

    let isinThrottle = false
    return function(args) {
        if (!isinThrottle) {
            isinThrottle = true
            fn.apply(this, [...args])

            setTimeout(() => {
                isinThrottle = false


            }, delay);
        }
    }
}