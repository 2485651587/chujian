//实现call apply bind

call和apply和bind的区别 

call和apply都说立即执行，bind返回一个函数
call接受，apply接受数组 


//实现call

案例

var obj = {
    value: "vortesnail",
  };
  
  function fn() {
    console.log(this.value);
  }
  
  fn.call(obj); // vortesnail


  //实现call

  Function.prototype.myCall = function (context, ...args) {
    // 如果没有传递 context，默认使用全局对象（在浏览器中是 window）
    context = context || globalThis;
    
    // 为 context 创建一个唯一的临时属性存储当前函数
    const fnSymbol = Symbol('fn');  // 使用 Symbol 避免与已有属性冲突
    context[fnSymbol] = this;  // this 指向当前调用 myCall 的函数
    
    // 使用 context 调用函数并传递参数
    const result = context[fnSymbol](...args);
    
    // 删除临时属性
    delete context[fnSymbol];
    
    return result;  // 返回函数的执行结果
};



//实现apply

Function.prototype.myApply = function (context, args) {
    // 如果没有传递 context，默认使用全局对象（在浏览器中是 window）
    context = context || globalThis;
    
    // 为 context 创建一个唯一的临时属性存储当前函数
    const fnSymbol = Symbol('fn');
    context[fnSymbol] = this;
    
    // 使用 context 调用函数，并传递参数数组
    const result = context[fnSymbol](...(args || []));  // 确保 args 是数组
    
    // 删除临时属性
    delete context[fnSymbol];
    
    return result;  // 返回函数执行结果
};
//实现bind 


Function.prototype.myBind =(context,args)=>{

    let th=this 

    return function(args2){
        th.apply(context,[...args,...args2])
    }

}




