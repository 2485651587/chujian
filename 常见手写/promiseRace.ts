Promise.prototype.myRace=function(args)=>{
    if(!args){
        return
    }
    return new Promise((resolve,reject)=>{
                    args?.forEach((v)=>{
            Promise.resolve(v).then((data)=>{
                resolve(data)
            }).catch((v)=>{
                reject(v)
            })
        })
    })
}