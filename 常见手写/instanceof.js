//原理。从原型链一路往上查找，直到等于父亲的prototype 


//if(a._proto_===b.prototype)=====>a为b的实例


// const instance =(Fn,fn)=>{
//     let proto=fn._proto_  

//     while(proto!==null){
//     if(proto===Fn.prototype){
//         return true
//     }
//      proto=proto._proto_  

//     }

//     return false 
// }

const instance = (Fn, fn) => {
    // 检查 fn 是否为对象类型
    if (typeof fn !== 'object' || fn === null) {
        return false; // 不是对象类型，直接返回 false
    }

    let proto = fn.__proto__;  // 使用 __proto__

    while (proto !== null) {
        if (proto === Fn.prototype) {
            return true;  // 找到匹配的原型
        }
        proto = proto.__proto__;  // 向上查找原型链
    }

    return false;  // 没有找到匹配的原型
};
