// 自定义 map 方法，使用 reduce 实现
Array.prototype.myMap = function(callback) {
    // 使用 reduce 进行数组遍历和累加
    return this.reduce((acc, currentValue, index, array) => {
      // 调用回调函数，获取新的元素值
      const mappedValue = callback(currentValue, index, array);
      // 将新值添加到累加器中（即新数组）
      acc.push(mappedValue);
      return acc;
    }, []); // 初始值为空数组
  };
  