//数组转树





// let arr = [
//   {id: 1, name: '部门1', pid: 0},
//   {id: 2, name: '部门2', pid: 1},
//   {id: 3, name: '部门3', pid: 1},
//   {id: 4, name: '部门4', pid: 3},
//   {id: 5, name: '部门5', pid: 4},

  
// ]


/**
 * 
 * [
    {
        "id": 1,
        "name": "部门1",
        "pid": 0,
        "children": [
            {
                "id": 2,
                "name": "部门2",
                "pid": 1,
                "children": []
            },
            {
                "id": 3,
                "name": "部门3",
                "pid": 1,
                "children": [
                    // 结果 ,,,
                ]
            }
        ]
    }
]

 */


//根据pid进行排序,pid代表层级

//递归实现 
function arrayToTree(arr, pid = 0) {
  const result = [];
  
  arr.forEach(item => {
      if (item.pid === pid) {
          const children = arrayToTree(arr, item.id);  // 递归查找子节点
          if (children.length) {
              item.children = children;
          } else {
              item.children = [];
          }
          result.push(item);
      }
  });

  return result;
}

const arr = [
{id: 1, name: '部门1', pid: 0},
{id: 2, name: '部门2', pid: 1},
{id: 3, name: '部门3', pid: 1},
{id: 4, name: '部门4', pid: 3},
{id: 5, name: '部门5', pid: 4},
];

console.log(JSON.stringify(arrayToTree(arr), null, 2),2222);


//迭代法


const array2tree=(arr)=>{
  let result =[]

  let map ={}

  arr.forEach(v=>{
    const {id,name,pid} =v 
    map[id]={
      pid:pid,
      name,
      children:[]
    }
  })

  arr.forEach(s=>{
    if(s.pid===0){ // 寻找根节点

      result.push(map[item.id])

    }else{ //不是根节点，将其加到其父节点的childeren中
    //其代表id，pid指向的是父节点，所以是
    //map[item.pid].children.push(map[item.id])

      map[item.pid].children.push(map[item.id])

    }
  })


  return result


}


//树转数组

function flatTree(tree) {
  const result = [];
  const stack = tree.map(node => ({ ...node, parentId: null })); // 初始化栈，根节点的 parentId 为 null

  while (stack.length > 0) {
    const node = stack.pop(); // 取出栈顶元素
    const { id, label, parentId, children } = node;

    // 将当前节点的 id、label 和 parentId 添加到结果数组中
    result.push({ id, label, parentId });

    // 如果有子节点，将子节点连同 parentId 加入栈
    if (children) {
      stack.push(...children.map(child => ({ ...child, parentId: id })));
    }
  }

  return result;
}

const tree = [
  {
    id: "1",
    label: "1",
    children: [
      { id: "1-1", label: "1-1" },
      { id: "1-2", label: "1-2" },
      { id: "1-3", label: "1-3" }
    ]
  },
  {
    id: "2",
    label: "2",
    children: [
      { id: "2-1", label: "2-1" },
      { id: "2-2", label: "2-2" },
      { id: "2-3", label: "2-3" }
    ]
  }
];

const flatResult = flatTree(tree);
console.log(flatResult);

