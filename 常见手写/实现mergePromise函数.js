// 实现mergePromise函数，把传进去的数组按顺序先后执行，并且把返回的数据先后放到数组data中。

/**
 * 
 * const time = (timer) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve()
    }, timer)
  })
}
const ajax1 = () => time(2000).then(() => {
  console.log(1);
  return 1
})
const ajax2 = () => time(1000).then(() => {
  console.log(2);
  return 2
})
const ajax3 = () => time(1000).then(() => {
  console.log(3);
  return 3
})

function mergePromise () {
  // 在这里写代码
}

mergePromise([ajax1, ajax2, ajax3]).then(data => {
  console.log("done");
  console.log(data); // data 为 [1, 2, 3]
});

// 要求分别输出
// 1
// 2
// 3
// done
// [1, 2, 3]

**/


// 现有一个 AJAX请求 function dataFetch() : promise,  有50%概率请求成功、50%概率请求失败。
// 需要设计一个并发控制函数 FetchAll(func: typeof dataFetch, count: number,  threshold: number ): promise  
// 实现以下功能点：
// ● 同时并发 count 个 func 请求函数
// ● 如果请求的失败数量 <   threshold ,  则返回 Promise.resolve 
// ● 如果请求的失败数量 >= threshold ,  则返回  Promise.reject 
// 代码如下:
function dataFetch()  {
    // do something
    // await random time
    return Math.random() > 0.5 ? Promise.resolve() : Promise.reject()
};
// 使用示例: 并发10次请求，最多可失败5次。
fetchAll(dataFetch, 10, 5)
    .then(() => console.log("successs"))
    .catch(() => console.error("error"));
function fetchAll (func, count, threshold)  {
    // 请补全你的代码
};



// Typescript 
// // any 与 unknown 的区别
// type SomeType1<T> = { [P in keyof T]?: T[P] }
// type SomeType2<T extends any[]> = T extends [infer A, ...infer rest] ? A : never
// // Typescript 在实际开发过程中的困扰 or 弊端


const fetchAll=(dataFetch,10,5)=>{
    let successsArr=[]
    let successsTimes=0 
    let failTimes =0
    return new Promise((resolve,reject)=>{
        for(let i=0;i<10;i++){
            dataFetch().then(res=>{
                successsTimes++
                if(successsTimes>=5){
                    resolve()
                }

            }).catch(err=>{
                failTimes++
                if(failTimes>=5){
                    reject()
                }
            })
        }

    })
  



}