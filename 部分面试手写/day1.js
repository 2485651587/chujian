//已知如下数组：
// var arr = [ [1, 2, 2], [3, 4, 5, 5], [6, 7, 8, 9, [11, 12, [12, 13, [14] ] ] ], 10];
// 编写一个程序将数组扁平化去并除其中重复部分数据，最终得到一个升序且不重复的数组


// const test =(arr,result=new Array())=>{
//     if(!result){

//     }
//     if(!Array.isArray(arr)){ //终止
//         return arr

//     }

//     for(let i =0;i<arr.length;i++){

//     }
// }



const test = (arr) => {

    let result = []

    const helper = (arr) => {
        if (!Array.isArray(arr)) {
            result.push(arr)
        }
        for (let i = 0; i < arr.length; i++) {
            if (!Array.isArray(arr[i])) {
                result.push(arr[i])
            } else {
                helper(arr[i])
            }
        }
    }
    helper(arr)

    return [...new Set(result)].sort((a, b) => a - b)
}


var arr = [
    [1, 2, 2],
    [3, 4, 5, 5],
    [6, 7, 8, 9, [11, 12, [12, 13, [14]]]], 10
];

test(arr)


//结合此题，手动实现数组的flat方法。
/**
 * 
 * flat接受一个可选参数，默认为1，
 */





Array.prototype.myFlat = function(num = 1) {
    let times = num

    let result = []
    const helper = (arr, times) => {
        if (times == -1 || !Array.isArray(arr)) { //当深度为0或者是非数组的时候，直接push
            result.push(arr)
        } else {

            arr.forEach(item => {
                if (!Array.isArray(item)) {
                    result.push(item)
                } else {
                    helper(item, times - 1)
                }
            })

        }
    }

    helper(this, times)


    return result

}