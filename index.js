

//
const invokeMostThreeRequest=async ()=>{


const arr=[]
arr.length=10
arr.fill(0)

let taskList=[]
arr.forEach((item,index)=>{ //构造异步请求
  taskList.push(new Promise((res,rej)=>{
    setTimeout(() => {
      res(index)
    }, index*100);
  }))
})


//利用promise；race记录最先执行完成的请求，并且将其移除

let invokedList=[]
let resultList=[] 
const maxCount=10 
const getInvoke=(data,count)=>{
  let list =[]
  data.forEach(item=>{
    if(list.length<count && item!==null){
      list.push(item)
    }
  })

  return list
}

while(invokedList.length!==10){
  const res=await Promise.race(getInvoke(taskList,3)) 
  resultList.push(res)
  invokedList.push(res)
  taskList[res]=null
  
}


console.log(120,resultList)




}

invokeMostThreeRequest()